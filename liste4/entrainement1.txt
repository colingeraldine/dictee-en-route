﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>TP : Un formulaire interactif</title>
</head>


<body> 

<center> 
<div id="logo">
      <p>DICTEE DE MOTS
      <font color="#FFFFFF">liste 3 </font></p>
</div>
<br><br>
</center>

    <form id="myForm">
	<table>
	<tr><td>
		<audio src="unbebe.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd1" id="pwd1" type="text" />
        <span id="rep1" class="tooltip">faux</span>
        <br /><br />
	</td><td>	
		<audio src="unhibou.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd2" id="pwd2" type="text" />
        <span id="rep2" class="tooltip">faux</span>
        <br /><br />
	</td></tr><tr><td>
		<audio src="unecabane.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd3" id="pwd3" type="text" />
        <span id="rep3" class="tooltip">faux</span>
        <br /><br />
	</td><td>
		<audio src="labiche.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd4" id="pwd4" type="text" />
        <span id="rep4" class="tooltip">faux</span>
        <br /><br />
	</td></tr><tr><td>	
		<audio src="bete.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd5" id="pwd5" type="text" />
        <span id="rep5" class="tooltip">faux</span>
        <br /><br />		
	</td><td>	
		<audio src="larbre.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd6" id="pwd6" type="text" />
        <span id="rep6" class="tooltip">faux</span>
        <br /><br />
	</td></tr><tr><td>	
		<audio src="blanc.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd7" id="pwd7" type="text" />
        <span id="rep7" class="tooltip">faux</span>
        <br /><br />		
	</td><td>
		<audio src="labranche.mp3" controls></audio>   <!-- changer le fichier son ici, le mettre dans le même répertoire -->
        <input name="pwd8" id="pwd8" type="text" />
        <span id="rep8" class="tooltip">faux</span>
        <br /><br />
	</td></tr><tr><td>
		
        <br /><br />	
	</td></tr></table>
		
    </form>

	
<style type="text/css">
body{
background-color:#9966FF}

#logo{
width:500px;
background-color:#FACC2E;
font-family:Comic Sans MS;
font-size:2em;
color:#7D0000;
text-align:center;
padding:0px
}

audio{
max-width:125px;}

td{
border : none;
padding-left:90px;
padding-top:15px;}

span{
min-width:40px;
}

.form_col {
    display: inline-block;
    margin-right: 15px;
    padding: 3px 0px;
    width: 200px;
    min-height: 1px;
    text-align: left;
}

input {
	margin-left: 20px;
	padding: 1px;
	border: 1px solid #CCC;
	border-radius: 2px;
	outline: none; /* Retire les bordures appliquées par certains navigateurs (Chrome notamment) lors du focus des éléments <input> */
	max-width:80px;
}
	
input:focus {
	border-color: rgba(82, 168, 236, 0.75);
	box-shadow: 0 0 8px rgba(82, 168, 236, 0.5);
}
	
.correct {
	border-color: rgba(68, 191, 68, 0.75);
}
	
.correct:focus {
	border-color: rgba(68, 191, 68, 0.75);
	box-shadow: 0 0 8px rgba(68, 191, 68, 0.5);
}
	
.incorrect {
	border-color: rgba(191, 68, 68, 0.75);
}
	
.incorrect:focus {
	border-color: rgba(191, 68, 68, 0.75);
	box-shadow: 0 0 8px rgba(191, 68, 68, 0.5);
}
	
.tooltip {
	display: inline-block;
	margin-left: 20px;
	padding: 2px 4px;
	border: 1px solid #555;
	background-color: #CCC;
	border-radius: 4px;
}


</style>




<script>
// La fonction ci-dessous permet de récupérer la "tooltip" qui correspond à notre input
function getTooltip(elements) {
    while (elements = elements.nextSibling) {
        if (elements.className === 'tooltip') {
            return elements;
        }
    }
    return false;
}

// Fonctions de vérification du formulaire, elles renvoient "true" si tout est ok
var check = {}; // On met toutes nos fonctions dans un objet littéral

check['pwd1'] = function() {
    var pwd1 = document.getElementById('pwd1'),
        tooltipStyle = getTooltip(pwd1).style;
    if (pwd1.value == "un bébé") {                    // changer le mot de correction ici
        pwd1.className = 'correct';
        document.getElementById("rep1").innerText="juste";
        return true;
    } else {
        pwd1.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd2'] = function() {
    var pwd2 = document.getElementById('pwd2'),
        tooltipStyle = getTooltip(pwd2).style;
    if (pwd2.value == "un hibou") {                    // changer le mot de correction ici
        pwd2.className = 'correct';
        document.getElementById("rep2").innerText="juste";
        return true;
    } else {
        pwd2.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd3'] = function() {
    var pwd3 = document.getElementById('pwd3'),
        tooltipStyle = getTooltip(pwd3).style;
    if (pwd3.value == "une cabane") {                    // changer le mot de correction ici
        pwd3.className = 'correct';
        document.getElementById("rep3").innerText="juste";
        return true;
    } else {
        pwd3.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd4'] = function() {
    var pwd4 = document.getElementById('pwd4'),
        tooltipStyle = getTooltip(pwd4).style;
    if (pwd4.value == "la biche") {                    // changer le mot de correction ici
        pwd4.className = 'correct';
        document.getElementById("rep4").innerText="juste";
        return true;
    } else {
        pwd4.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd5'] = function() {
    var pwd5 = document.getElementById('pwd5'),
        tooltipStyle = getTooltip(pwd5).style;
    if (pwd5.value == "bête") {                    // changer le mot de correction ici
        pwd5.className = 'correct';
        document.getElementById("rep5").innerText="juste";
        return true;
    } else {
        pwd5.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd6'] = function() {
    var pwd6 = document.getElementById('pwd6'),
        tooltipStyle = getTooltip(pwd6).style;
    if (pwd6.value == "l'arbre") {                    // changer le mot de correction ici
        pwd6.className = 'correct';
        document.getElementById("rep6").innerText="juste";
        return true;
    } else {
        pwd6.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd7'] = function() {
    var pwd7 = document.getElementById('pwd7'),
        tooltipStyle = getTooltip(pwd7).style;
    if (pwd7.value == "blanc") {                    // changer le mot de correction ici
        pwd7.className = 'correct';
        document.getElementById("rep7").innerText="juste";
        return true;
    } else {
        pwd7.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};

check['pwd8'] = function() {
    var pwd8 = document.getElementById('pwd8'),
        tooltipStyle = getTooltip(pwd8).style;
    if (pwd8.value == "la branche") {                    // changer le mot de correction ici
        pwd8.className = 'correct';
        document.getElementById("rep8").innerText="juste";
        return true;
    } else {
        pwd8.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }
};



// Mise en place des événements

(function() { // Utilisation d'une IIFE pour éviter les variables globales.
    var myForm = document.getElementById('myForm'),
        inputs = document.querySelectorAll('input[type=text], input[type=password]'),
        inputsLength = inputs.length;
    for (var i = 0; i < inputsLength; i++) {
        inputs[i].addEventListener('keyup', function(e) {
            check[e.target.id](e.target.id); // "e.target" représente l'input actuellement modifié
        });
    }
    myForm.addEventListener('submit', function(e) {
        var result = true;
        for (var i in check) {
            result = check[i](i) && result;
        }
        if (result) {
            alert('Le formulaire est bien rempli.');
        }
        e.preventDefault();
    });

    myForm.addEventListener('reset', function() {
        for (var i = 0; i < inputsLength; i++) {
            inputs[i].className = '';
        }
        deactivateTooltips();
    });
})();



</script>
</body>
</html>